Gem::Specification.new do |s|
  s.name = "editable-image"
  s.version = "2.0.0"
  s.date = "2015-01-26"
  s.summary = "Simplified interface to web-based image editors."
  s.email = ["tjstankus@gmail.com", "cnk@caltech.edu"]
  s.homepage = "https://bitbucket.org/caltech-imss-ads/editable-image"
  s.description = "Simplified interface to web-based image editors. Original version: https://github.com/haikuwebdev/editable-image later updated to add PicMonkey and Pixlr support"
  s.has_rdoc = false
  s.authors = ["TJ Stankus", "Cynthia Kiser"]
  s.license = 'MIT'
  s.files = ["editable-image.gemspec", "README", "lib/multipart.rb", "lib/editable-image.rb", "lib/editable-image/exceptions.rb", "lib/picnik/picnik.rb", "lib/picmonkey/picmonkey.rb", "lib/pixlr/pixlr.rb"]
  s.test_files = ["test/picnik_test.rb", "test/files/logo.gif", "test/files/test.txt"]
  s.add_runtime_dependency("mime-types", ["~> 2.4"])
  s.add_development_dependency("minitest", ["~> 5.4"])
  s.add_development_dependency("minitest-spec", ["~> 0.0"])
end
