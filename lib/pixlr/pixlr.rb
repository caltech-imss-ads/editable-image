require 'erb'
include ERB::Util

module EditableImage
  class Pixlr

    PIXLR_API_PARAMETERS = %w(
      referrer exit image title type method target maxheight maxwidth
      icon redirect locktarget locktitle locktype copy quality wmode
    )

    def self.url(parameters)
      "http://pixlr.com/express" + urlencode_params(parameters)
    end

    private
    
    def self.urlencode_params(params)                                      
      arg_list = [] 
      params.each do |key, value|                              
        arg_list << url_encode(key) + "=" + url_encode(value)
      end
      return "?" + arg_list.join("&")
    end                                                        
    
  end
end
