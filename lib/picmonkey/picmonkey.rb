module EditableImage
  class Picmonkey

    PICMONKEY_API_PARAMETERS = %w(
      apikey sig expires import returntype title export export_agent
      export_field export_method export_title redirect imageid 
      out_format out_quality out_maxsize out_maxwidth out_maxheight
      host_name page close_target expand_button
    )

    PICMONKEY_URL = 'http://www.picmonkey.com/service'

    def self.url(parameters)
      raise EditableImage::InvalidParametersError unless (api_key_in?(parameters) && import_in?(parameters))
      url = URI(PICMONKEY_URL)
      url.query = URI.encode_www_form(scrub_parameters(parameters))
      url.to_s
    rescue EditableImage::InvalidParametersError => e
      raise e, "Parameters must include the apikey and image url."
    end

    def self.post_url(full_filename, parameters)
      raise EditableImage::InvalidParametersError unless api_key_in?(parameters)
      url = ''
      File.open(full_filename) do |file|
        raise EditableImage::InvalidFileTypeError unless image?(full_filename)
        http = Net::HTTP.new('www.picmonkey.com')
        http.start do |http|
          request = Net::HTTP::Post.new('/service/')
          request.multipart_params = request_parameters(file, parameters)
          response = http.request(request)
          url = response.body
        end
      end
      url
    rescue EditableImage::InvalidFileTypeError => e
      raise e, "File must be an image."
    rescue EditableImage::InvalidParametersError => e
      raise e, "Parameters must include the apikey."
    rescue Errno::ENOENT => e
      raise EditableImage::InvalidFilenameError.new, e.message
    end

    private

    def self.request_parameters(file, parameters)
      required_parameters = { :import => 'image_data', :image_data => file, :returntype => 'text' }
      scrub_parameters(required_parameters.merge(parameters))
    end

    def self.scrub_parameters(parameters)
      scrubbed_parameters = {}
      parameters.each do |key, value|
        scrubbed_parameters[parameter_key(key)] = value
      end
      scrubbed_parameters
    end

    def self.parameter_key(key)
      key = key.to_s
      PICMONKEY_API_PARAMETERS.include?(key) ? '_' + key : key
    end

    def self.image?(filename)
      MIME::Types.of(filename).first.media_type == 'image'
    end

    # NOTE: Generalize, DRY up if needed. For now YAGNI.
    def self.api_key_in?(parameters)
      scrub_parameters(parameters).has_key?('_apikey')
    end

    def self.import_in?(parameters)
      scrub_parameters(parameters).has_key?('_import')
    end

  end
end
