require 'minitest/spec'
require 'minitest/autorun'
require File.dirname(__FILE__) + '/../lib/editable-image'

describe EditableImage::Picnik do

  describe "Request parameters" do
    before do
      EditableImage::Picnik.class_eval do
        def self.public_request_parameters(*args)
          request_parameters(*args)
        end
      end
      @file = File.open("files/logo.gif")
      parameters = {:apikey => 'test_picnik_apikey'}
      @request_parameters = EditableImage::Picnik.public_request_parameters(@file, parameters)
    end

    after do
      @file.close
    end

    it "include _import" do
      assert @request_parameters.keys.include?('_import')
    end

    it "have correct value for _import" do
      assert_equal 'image_data', @request_parameters['_import']
    end

    it "include image_data" do
      assert @request_parameters.keys.include?('image_data')
    end

    it "not have nil image_data" do
      @request_parameters['image_data'].wont_be_nil
    end

    it "include _returntype" do
      assert @request_parameters.keys.include?('_returntype')
    end

    it "have returntype of text" do
      assert_equal 'text', @request_parameters['_returntype']
    end

  end

  describe "Parameter key" do
    before do
      EditableImage::Picnik.class_eval do
        def self.public_parameter_key(*args)
          parameter_key(*args)
        end
      end
    end

    it "be stringified" do
      assert_equal 'foo', EditableImage::Picnik.public_parameter_key(:foo)
    end

    it "add underscore for Picnik API parameter" do
      assert_equal '_apikey', EditableImage::Picnik.public_parameter_key(:apikey)
    end

    it "not add underscore for non-Picnik API parameter" do
      assert_equal 'foo', EditableImage::Picnik.public_parameter_key('foo')
    end

    it "not add underscore for Picnik API parameter that already has one" do
      assert_equal '_apikey', EditableImage::Picnik.public_parameter_key('_apikey')
    end

    it "keep underscore for non Picnik API parameter" do
      assert_equal '_method', EditableImage::Picnik.public_parameter_key('_method')
    end
  end

  describe "A bad filename" do
    before do
      @filename = 'bad_filename'
      @parameters = {:apikey => 'test'}
    end

    it "raise EditableImage::InvalidFilenameError exception" do
      proc {
        EditableImage::Picnik.url(@filename, @parameters)
      }.must_raise EditableImage::InvalidFilenameError
    end

    it "have exception message containing 'no such file'" do
      begin
        EditableImage::Picnik.url(@filename, @parameters)
      rescue EditableImage::InvalidFilenameError => e
        assert_match /no such file/i, e.message
      end
    end
  end

  describe "A non-image file" do
    before do
      @non_image_filename = File.expand_path("files/test.txt")
      @parameters = {:apikey => 'test'}
    end

    it "raise EditableImage::InvalidFileTypeError exception" do
      proc {
        EditableImage::Picnik.url(@non_image_filename, @parameters)
      }.must_raise EditableImage::InvalidFileTypeError
    end

    it "have exception message containing 'foo'" do
      begin
        EditableImage::Picnik.url(@non_image_filename, @parameters)
      rescue EditableImage::InvalidFileTypeError => e
        assert_match /must be an image/i, e.message
      end
    end
  end

  describe "A request for url without apikey in parameters" do

    it "raise EditableImage::InvalidParametersError" do
      proc {
        EditableImage::Picnik.url('some_filename', {})
      }.must_raise EditableImage::InvalidParametersError
    end

    it "raise EditableImage::InvalidParametersError exception with message containing 'must include the apikey'" do
      begin
        EditableImage::Picnik.url('some_filename', {})
      rescue EditableImage::InvalidParametersError => e
        assert_match /must include the apikey/i, e.message
      end
    end

  end

end
